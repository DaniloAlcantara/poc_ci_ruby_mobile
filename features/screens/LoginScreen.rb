class LoginScreen < Utils
    
    include ::RSpec::Matchers

    #Elementos/Objetos

    def logo_qaninja
        return localizar(:id, "io.qaninja.android.twp:id/mi_image")
    end

    def btn_comecar
        return localizar(:xpath, "//android.widget.Button[@text='COMEÇAR']")
    end

    def btn_hamburguer
        return localizar(:accessibility_id, "Open navigation drawer")
    end

    def opc_forms
        return localizar(:uiautomator, 'new UiSelector().className("android.widget.FrameLayout").index(2)')
    end

    def opc_login
        return localizar(:xpath, "//android.widget.TextView[@text='LOGIN']")
    end

    def campo_email
        return localizar(:id, "io.qaninja.android.twp:id/etEmail")
    end

    def campo_senha
        return localizar(:id, "io.qaninja.android.twp:id/etPassword")
    end

    def btn_Entrar
        return localizar(:id, "io.qaninja.android.twp:id/btnSubmit")
    end

    def toast_message
        return localizar(:xpath, "//android.widget.Toast")
    end
    
    
    

    
    #===========================================================================================================

    #Ações/Metodos
    def clicarBotaoComecar
        tap(btn_comecar)
    end

    def clicarHamburguer
        tap(btn_hamburguer)
        esperar(opc_forms)
    end

    def clicarForms
        tap(opc_forms)
        esperar(opc_login)
    end

    def clicarLogin
        tap(opc_login)
    end

    def realizarLogin(email, senha)
        escrever(campo_email, email)
        escrever(campo_senha, senha)
        tap(btn_Entrar)
    end

    #usado no hooks
    def loginBasico(email, senha)
        btn_SouCliente.click
        txt_Email.send_keys email 
        txt_Senha.send_keys senha
        btn_Entrar.click
    end

end