
Dado('que abro o App') do
  expect(login.logo_qaninja.displayed?).to be true
end

Dado('pressiono o botao comecar') do
  login.clicarBotaoComecar
end

Dado('abro o menu') do
  login.clicarHamburguer
end

Dado('seleciono a opcao forms') do
  login.clicarForms
end

Quando('seleciono a opcao Login') do
  login.clicarLogin
end

Quando('informo o usuario {string} e senha {string}') do |email, senha|
  login.realizarLogin(email, senha)
end

Então('é apresentada mensagem de sucesso') do
  expect(login.toast_message.text).to eql "Show! Suas credenciais são validas."
end