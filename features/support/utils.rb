
class Utils 
    # Métodos genericos para serem usados em qualquer parte do aplicativo

    def  tap_coordenada(x,y)
        Appium::TouchAction.new.tap(x: x, y: y).perform
    end

    def scrollEsquerdaParaDireita(atributo, s_x, s_y, o_x, o_y, duration)
        find_element(:id, atributo)
        coord = { start_x: s_x , start_y: s_y, offset_x: o_x, offset_y: o_y, duration: duration}
        Appium::TouchAction.new.swipe(coord).perform 
        sleep 2
    end


    def tap(elemento)
        elemento.click
    end
 
    def escrever(elemento,texto)
         elemento.send_keys texto
    end
 
    def esperar(elemento)
         $wait.until{elemento}
    end
 
    def duplo_tap(elemento)
         2.times{tap(elemento)}
    end

    def esconder_teclado
         hide_keyboard
    end

    def confirmar_teclado
         press_keycode(66)
    end

    def localizar(atributo,identificador)
        find_element(atributo,identificador)
    end


    

        
end    
        