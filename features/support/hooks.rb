#Acontece antes de todos os cenarios
Before do |scenario|
    #Iniciar o driver
    $driver.start_driver
    #Espera implicita para encontrar os elementos
    $driver.manage.timeouts.implicit_wait = 90

end

#login que sera usado para outras funcionalidades, so roda pra quem tem a tag @loginBasico
Before("@loginBasico") do
  login.loginBasico("", "")
end



#Acontede depois de todos cenarios, configurar para tirar screnchot
After do |scenario|
    binary_shot = driver.screenshot_as(:base64)
  
    temp_shot = "logs/temp_shot.png"
  
    File.open(temp_shot, "wb") do |f|
      f.write(Base64.decode64(binary_shot).force_encoding("UTF-8"))
    end
  
    Allure.add_attachment(
      name: "screenshot",
      type: Allure::ContentType::PNG,
      source: File.open(temp_shot),
    )

    driver.quit_driver
end
  