require 'appium_lib'
require 'cucumber'
require 'rspec/expectations'
require 'selenium-webdriver'
require 'appium_lib_core'
require 'allure-cucumber'
require 'fileutils'
require 'report_builder'

require_relative '../support/page_helper.rb'

#Deixando as classes globais no projeto
World(PageHelper)

#Espera explicita
$wait = Selenium::WebDriver::Wait.new(timeout: 30)

#Definindo as capabilities 
def caps
  {
  
  caps: {

    automationName: "UIAutomator2",
    platformName:"Android",
    deviceName: "emulator",
    platformVersion:"11",
    udid:"emulator-5554",
    app:"app//app_twp.apk",
    noReset:false,
    fullReset:false,
    isHeadless:false
 },
  
  appium_lib: {
   # host: 192.168.99.100,
    port: 4723,
    wait: 30
    }
  }
  
end
  
#Definindo o driver
Appium::Driver.new(caps, true)

Appium.promote_appium_methods Object


#caps = Appium.load_appium_txt file: File.expand_path("caps/android.txt", __dir__), verbose: true

# Limpa os logs e os screenshots
FileUtils.rm_f(Dir.glob("logs/*.json"))
FileUtils.rm_f(Dir.glob("logs/*.png"))


#Configuração Geral do Allure
AllureCucumber.configure do |config|
  config.results_directory = "C:/Automacao/projeto-android/logs"
  config.clean_results_directory = true
  config.logging_level = Logger::INFO
end
